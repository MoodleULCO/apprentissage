# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 10:07:06 2020

@author: bayar
"""
import random
#import sys
#import matplotlib.path as mpath
#import matplotlib.patches as mpatches
#import matplotlib.pyplot as plt
import numpy as np
import points_generator as pg


NB_POINT = 100
LEARNING_STEP = 0.01

class Neurone:
    def __init__(self):
        self.biais = 0.5
        self.out = 0
        self.weight = [np.random.random(), np.random.random()]

    def outNeurone(self, valeurs):
        sigma = sum((self.weight * np.asarray(valeurs))) - self.biais

        if (sigma > 0):
            return 1

        return -1
    
    def updateNeurone(self, valeurs, etiquette):
        self.biais = self.biais + LEARNING_STEP  * (etiquette - self.out) * -0.5
        for i in range(len(self.weight)):
            self.weight[i] += LEARNING_STEP * (etiquette - self.out) * valeurs[i]
        
pg.writePoints(NB_POINT)

n = Neurone()

for i in range(100):
    points = pg.getPoints()
    nbErreur = 0
    
    for i in range (NB_POINT):
    
        valeur = [float(points[i][0]), float(points[i][1])]
    
        etiq = int(points[i][2])
        
        if n.outNeurone(valeur) != etiq:
            n.updateNeurone(valeur,etiq)
            nbErreur += 1
    print("Erreur : {}".format(nbErreur))
