# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 09:21:32 2020

@author: bayar
"""

import random

def randomPoints(NbPt):
    liste = []
    
    for i in range (NbPt):        
        points = []
        
        x1 = random.uniform(0,1)
        x2 = random.uniform(0,1)
        
        if (x1+x2-1) > 0:
            etiq = 1
        else:
            etiq = -1
        
        points.append(x1)
        points.append(x2)
        points.append(etiq)
        liste.append(points)
    
    return liste
    
def writePoints(NbPt):
    fichier = open("data.csv", "w")
    liste = randomPoints(NbPt)
        
    for i in range (NbPt):
        fichier.write(str(liste[i][0]) + " " + str(liste[i][1]) + " " + str(liste[i][2]) + "\n")
    
    fichier.close()
    
def getPoints():
    fichier = open("data.csv","r")
    lines = fichier.readlines()
    fichier.close()

    liste=[]
    

    for line in lines:
        line = line.rstrip("\n")
        x = line.split(" ")
        liste.append(x)

    return liste




