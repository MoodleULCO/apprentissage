# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 09:30:47 2020

@author: bayar
"""

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from math import sqrt;
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import mean_absolute_error, mean_squared_error

def encode (data, col):
    le = LabelEncoder()
    
    unique_values = list(data[col].unique())
    le_fitted = le.fit(unique_values)
    
    values = list(data[col].values)
    values_transformed = le.transform(values)
    
    data[col] = values_transformed  
    
def analyze_good_employees(data):
    # On calcule toutes les moyennes des caractéristiques qui nous intéressent
    averages = data.mean() # créer un data contenant toutes les moyennes pour tout
    # puis on s'intéresse en particulier aux caractéristiques qui nous font sens :
    average_last_evaluation = averages['last_evaluation'] 
    average_project = averages['number_project']
    average_montly_hours = averages['average_montly_hours']
    average_time_spend = averages['time_spend_company']

    # On va filtrer le dataframe pour ne garder que les exemples qui ont des caractéristiques 
    # supérieures à la moyenne --> on veut les meilleurs employés
    good_employees = data[data['last_evaluation'] > average_last_evaluation]
    good_employees = good_employees[good_employees['number_project'] > average_project]
    good_employees = good_employees[good_employees['average_montly_hours'] > average_montly_hours]
    good_employees = good_employees[good_employees['time_spend_company'] > average_time_spend]

    # seaborn permet de faire des beaux plots de visualisation de données
    sns.set()
    plt.figure(figsize=(15, 8))
    plt.hist(data['left'])
    print(good_employees.shape)
    sns.heatmap(good_employees.corr(), vmax=0.5, cmap="PiYG")
    plt.title('Correlation matrix')
    plt.show()

data = pd.read_csv("data/human_resources.csv")

encode(data, 'sales')
encode(data, 'salary')

analyze_good_employees(data)
    
xTrain, xTest = train_test_split(data, test_size=0.3);

yTrain = xTrain.pop('left')
yTest = xTest.pop('left')

classifier = MLPClassifier()
classifier.fit(xTrain, yTrain)
print("Train score: {}, Test score {}".format(classifier.score(xTrain, yTrain), classifier.score(xTest, yTest)))