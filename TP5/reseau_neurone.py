# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 15:32:20 2020

@author: bayar
"""

import pandas as pd

from math import sqrt;
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import mean_absolute_error, mean_squared_error

def encode (data, col):
    le = LabelEncoder()
    
    unique_values = list(data[col].unique())
    le_fitted = le.fit(unique_values)
    
    values = list(data[col].values)
    values_transformed = le.transform(values)
    
    data[col] = values_transformed

data = pd.read_csv("data/human_resources.csv")

encode(data, 'sales')
encode(data, 'salary')

print(data)

xTrain, xTest = train_test_split(data, test_size=0.5);

mc = MLPClassifier()

yTrain = xTrain.pop('left')
yTest = xTest.pop('left')

mc.fit(xTrain, yTrain)

yPred = mc.predict(xTest)

print('Score: ', mc.score(xTest, yTest))

print('MAE: ', mean_absolute_error(yTest, yPred))
print('MSE: ', sqrt(mean_squared_error(yTest, yPred)))