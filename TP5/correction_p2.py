# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 10:04:39 2020

@author: bayar
"""
import pandas as pd

from math import sqrt;
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_absolute_error, mean_squared_error

data = pd.read_csv("data/hour.csv")
print(data)

del(data['dteday'])
del(data['instant'])

xTrain, xTest = train_test_split(data, test_size=0.3);

mc = MLPRegressor()

yTrain = xTrain.pop('cnt')
yTest = xTest.pop('cnt')

mc.fit(xTrain, yTrain)

yPred = mc.predict(xTest)

print('Score: ', mc.score(xTest, yTest))

print('MAE: ', mean_absolute_error(yTest, yPred))
print('MSE: ', sqrt(mean_squared_error(yTest, yPred)))