from pandas import read_csv
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix

df = read_csv('iris.csv')

print('Nombre de classes:', df['Species'].value_counts().count())

print('\n')

print('Caractéristiques descriptives')
df.info()

print('\n')

print('Nombre d\'exemples de chaque classe:')
print(df['Species'].value_counts())

print('\n')

print('Nombre d\'exemples:', df.count())

print('\n')

print('Organisation: par espece')

print('\n')

print('-------')

xTrain, xTest = train_test_split(df, test_size=0.5);

kn = KNeighborsClassifier()

yTrain = xTrain.pop('Species')
yTest = xTest.pop('Species')

kn.fit(xTrain, yTrain)

yPred = kn.predict(xTest)

print('Score: ', kn.score(xTest, yTest))
print('Confusion Matrix:\n', confusion_matrix(yTest, yPred))