from pandas import read_csv

from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsRegressor
from sklearn.metrics import confusion_matrix
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from sklearn.preprocessing import StandardScaler

from math import sqrt;

df = read_csv('auto-mpg.data.csv')

df = df.drop('name', axis=1)

xTrain, xTest = train_test_split(df, test_size=0.5);

kn = KNeighborsRegressor()

yTrain = xTrain.pop('mpg')
yTest = xTest.pop('mpg')

kn.fit(xTrain, yTrain)

yPred = kn.predict(xTest)

print('Score: ', kn.score(xTest, yTest))

print('MEA: ', mean_absolute_error(yTest, yPred))
print('MSE: ', sqrt(mean_squared_error(yTest, yPred)))

print('----')

kn2 = KNeighborsRegressor()

scaler = StandardScaler()
xTrain = scaler.fit_transform(xTrain);
xTest = scaler.fit_transform(xTest);

kn2.fit(xTrain, yTrain)

yPred = kn2.predict(xTest)

print('Score: ', kn2.score(xTest, yTest))

print('MEA: ', mean_absolute_error(yTest, yPred))
print('MSE: ', sqrt(mean_squared_error(yTest, yPred)))