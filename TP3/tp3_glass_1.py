import pandas as pd
from sklearn import tree

def main():
	data = pd.read_csv("glass.csv")
	print(data)
	print(data['Type'])
	x_train = data
	y_train = data['Type']
	del x_train['Id']
	del x_train['Type']

	classifier = tree.DecisionTreeClassifier(criterion='entropy')
	classifier.fit(x_train, y_train)

	tree.export_graphviz(classifier, out_file='tree.dot', feature_names=['refractive index','Sodium','Magnesium','Aluminium','Silicon','Potassium','Calcium','Barium','Iron'])

if __name__ == '__main__':
	main()