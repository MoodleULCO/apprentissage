import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn import tree
from sklearn.metrics import confusion_matrix

def main():
	data = pd.read_csv("glass.csv")
	del data['Id']

	xTrain, xTest = train_test_split(data, test_size=0.5);

	yTrain = xTrain['Type']
	yTest = xTest['Type']

	del xTrain['Type']
	del xTest['Type']

	classifier = tree.DecisionTreeClassifier(
		criterion='entropy',
		max_depth=6,
		min_samples_leaf=2
	)
	classifier.fit(xTrain, yTrain)

	yPred = classifier.predict(xTest)

	print('Train Score: ', classifier.score(xTrain, yTrain))
	print('Test Score: ', classifier.score(xTest, yTest))
	print('Confusion Matrix:\n', confusion_matrix(yTest, yPred))

	tree.export_graphviz(classifier, out_file='tree.dot', feature_names=['refractive index','Sodium','Magnesium','Aluminium','Silicon','Potassium','Calcium','Barium','Iron'])

if __name__ == '__main__':
	main()