import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn import tree
from sklearn.metrics import confusion_matrix
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score

from math import sqrt;

def main():
	data = pd.read_csv("winequality-red.csv")

	xTrain, xTest = train_test_split(data, test_size=0.5);

	yTrain = xTrain['quality']
	yTest = xTest['quality']

	del xTrain['quality']
	del xTest['quality']

	classifier = tree.DecisionTreeRegressor(
		max_depth=2
	)
	classifier.fit(xTrain, yTrain)

	yPred = classifier.predict(xTest)

	print('Coefficient of determination: ', r2_score(yTest, yPred))
	print('MEA: ', mean_absolute_error(yTest, yPred))
	mse = mean_squared_error(yTest, yPred);
	print('MSE: ', mse, ',' , sqrt(mse))

	tree.export_graphviz(classifier, out_file='tree.dot', feature_names=['fixed acidity','volatile acidity','citric acid','residual sugar','chlorides','free sulfur dioxide','total sulfur dioxide','density','pH','sulphates','alcohol'])

if __name__ == '__main__':
	main()