import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Strategie {
	
	private static final int MIN_ESPERANCE = -10;
	private static final int MAX_ESPERANCE = 10;
	
	private static final int MIN_VARIANCE = 0;
	private static final int MAX_VARIANCE = 10;
	
	public Strategie() {}
	
	public ArrayList<Manchot> creerManchots(int nb) {
		Random r = new Random();
		double esperance = 0.0;
		double variance = 0.0;
		ArrayList<Manchot> list = new ArrayList<Manchot>();
		
		for(int i = 0; i < nb; i++) {
			esperance = MIN_ESPERANCE + (MAX_ESPERANCE - MIN_ESPERANCE) * r.nextDouble();
			variance = MIN_VARIANCE + (MAX_VARIANCE - MIN_VARIANCE) * r.nextDouble();
			Manchot m = new Manchot(esperance, variance);
			list.add(m);
		}
		
		return list;
	}
	
	public double rechercheAleatoire(int nbIterations, ArrayList<Manchot> manchots) {
		double sum = 0.0;
		
		Random r = new Random();
		for(int i = 0; i < nbIterations; i++) {
			sum += manchots.get(r.nextInt(manchots.size())).tirerBras();
		}
		return sum;
	}
	
	public double rechercheGloutonne(int nbIterations, ArrayList<Manchot> manchots) {
		double sum = 0.0;
		int bestManchot = 0;
		double bestScore = 0.0;
		
		for(int i = 0; i < manchots.size(); i++) {
			double score = manchots.get(i).tirerBras();
			
			if(score > bestScore) {
				bestScore = score;
				bestManchot = i;
			}
			
			sum += score;
			nbIterations--;
		}
		
		for(int i = 0; i < nbIterations; i++) {
			sum += manchots.get(bestManchot).tirerBras();
		}
	
		return sum;
	}
	
	public double rechercheUCB(int nbIterations, ArrayList<Manchot> manchots, double k) {
		int nbIterationsIni = nbIterations;
		double sum;
		Map<Integer, Integer> tiragesManchots = new HashMap<>();
		Map<Integer, Double> scoresManchots = new HashMap<>();
		
		for(int i = 0; i < manchots.size(); i++) {
			tiragesManchots.put(i, 1);
			scoresManchots.put(i, manchots.get(i).tirerBras());
			nbIterations--;
		}
		
		sum = getSomme(scoresManchots);
		
		for(int i = 0; i < nbIterations; i++) {
			int manchot = getMeilleurUCB(scoresManchots, tiragesManchots, nbIterationsIni, k);

			tiragesManchots.put(manchot, tiragesManchots.get(manchot) + 1);
			double score = manchots.get(manchot).tirerBras();
			scoresManchots.replace(manchot, scoresManchots.get(manchot) + score);
			sum += score;
		}
		
		System.out.println("Repartitions des tirages : " + tiragesManchots.values().toString());
		
		return sum;
	}
	
	private int getMeilleurUCB(Map<Integer, Double> scoresManchots, Map<Integer, Integer> tiragesManchots, int n, double k) {
		double bestUCB = 0.0;
		int bestManchot = 0;
		
		for(int i = 0; i < scoresManchots.size(); i++) {
			double UCB = scoresManchots.get(i) + k*Math.sqrt(Math.log(n)/tiragesManchots.get(i));
			if(UCB > bestUCB) {
				bestUCB = UCB;
				bestManchot = i;
			}
		}
		
		return bestManchot;
	}
	
	private double getSomme(Map<Integer, Double> scoresManchots) {
		double sum = 0.0;
		
		for(int i = 0; i < scoresManchots.size(); i++) {
			sum  += scoresManchots.get(i);
		}
		
		return sum;
	}
}