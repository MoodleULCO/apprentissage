public class Manchot {
	private double esperance;
	private double variance;
	
	public Manchot(double esperance, double variance) {
		this.esperance = esperance;
		this.variance = variance;
	}
	
	public double tirerBras() {
		double rand1 = Math.random();
		double rand2 = Math.random();
		
		return this.variance * Math.sqrt(-2.0 * Math.log(rand1)) * Math.cos(2 * Math.PI * rand2) + this.esperance;
	}
}
