import java.util.ArrayList;

public class UCB {
	private static final int NB_ITERATIONS = 15000;
	
	public static void main(String[] args) {
		Strategie strat = new Strategie();
		double k = Math.sqrt(2.0);

		for(int i = 0 ; i < 5; i++) {
			ArrayList<Manchot> manchots = strat.creerManchots(15);
			System.out.println("Recherche al�atoire : " + strat.rechercheAleatoire(NB_ITERATIONS, manchots));
			System.out.println("Recherche gloutonne : " + strat.rechercheGloutonne(NB_ITERATIONS, manchots));
			System.out.println("Recherche UCB : " + strat.rechercheUCB(NB_ITERATIONS, manchots, k));
			System.out.println();
		}
	}
}
