#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
x = np.array([[6], [8], [10], [14], [18]])
y = [7, 9, 13, 17.5, 18]
plt.plot(x,y, 'k.')
plt.axis([0,25,0,25])
plt.xlabel("Taille pizza")
plt.ylabel("Prix")
reg = LinearRegression().fit(x, y)
scr = reg.score(x, y)
print(scr)
yl = reg.predict(x)
plt.plot(x,yl)
plt.show()
mean = np.mean((y-yl)**2)
print(mean)
var = np.var(x, ddof=1)
print(var)
cov = np.cov(x.transpose().y)[0][1]
print(cov)