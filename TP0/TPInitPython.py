#!/usr/bin/python3
import pandas
import matplotlib.pyplot as plt
# Définition de la fonction comme
def somme(a,b):
    """
	Retourne la somme des deux nombres passés en paramètre
    """
    return a + b
# Usage
# help(somme)
# Help on function somme in module __main__:
# 
# somme(a, b)
# Retourne la somme des deux nombres passés en paramètre
# (END)
# parcourt d'une liste
for i in [1, 2, 3, 4, 5]:
    print(i) # affichage de la variable i
for j in [1, 2, 3, 4, 5]:
    print(j)
    print(i+j)
print("Fin de bloc")
chaine1 = 'une chaine'
chaine2 = "une autre chaine"
taille = len(chaine1)
print("La chaine 1 est {0} et a une taille de {1} la chaine 2 est {2}"
    .format(chaine1,taille,chaine2))
chaine3 = chaine1 + chaine2
print("La chaine 3 est {}".format(chaine3))
bjr = 'bonjour'
print(bjr[1])
print(bjr[0:2])
print(bjr[2:])
li = []
li.append(1)
li.append("bonjour")
autre_li = [1, 2, 3, 4, 5]
liste_de_liste = [ li, autre_li ]
autre_li[2:4]
del autre_li[3]
autre_li[2:4]
print(autre_li)
autre_li[-1]
autre_li[-2]
len(li)
li.extend(autre_li)
def square_and_cube(x):
    return(x*x,x*x*x)
a,b = square_and_cube(3)
print(a)
print(b)
_,c = square_and_cube(2)
print(c)
d = dict()
nombres = { "un" : 0, "deux" : 2, "dix" : 10 }
nombres["un"] = 1
nombres["cinq"] = 5
nombres.get("deux")
nombres.keys()
nombres.values()
nombres.items()
for k,v in nombres.items():
    print("La valeur de la clé {} est {}".format(k,v))
ens = set([1, 2, 2, 3, 4, 5])
ens2 = set([1, 3, 5, 7])
print(ens & ens2)
print(ens | ens2)
print(ens ^ ens2)
temp = 10
if temp > 25:
    print("Il fait chaud")
elif temp < 10:
    print("Je mets un pull")
else:
    print("J'hesite")
x = 0
while x < 10:
    print(x)
    x += 1
for x in range(10):
    print(x)
for i in range(2,4):
    print(i)
for couleur in ["rouge", "vert", "bleu", "jaune"]:
    print("{} est une couleur".format(couleur))
def somme(a,b):
    print("{} + {} = {}".format(a,b,a+b))
somme(3,4)
somme(a=3,b=4)
somme(b=4,a=3)
x = 42
def set_X(a):
    x = a
    x += 1
print(x)
def set_global_x(a):
    global x
    x = a
    x += 1
print(x)
