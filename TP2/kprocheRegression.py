import pandas as pa
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsRegressor
from sklearn.metrics import confusion_matrix
from sklearn.metrics import mean_absolute_error, mean_squared_error,r2_score
from sklearn.preprocessing import StandardScaler
bdd = pa.read_csv("./auto-mpg.csv")
del(bdd['name'])
Scale = StandardScaler()
train,test = train_test_split(bdd,shuffle=True)
x_train = train
y_train = train['mpg']
del(x_train['mpg'])
x_trainScale = Scale.fit_transform(x_train)
x_test = test
y_test = test['mpg']
del(x_test['mpg'])
x_testScale = Scale.fit_transform(x_test)
neighb = KNeighborsRegressor(n_neighbors=8)
trainFit = neighb.fit(x_trainScale,y_train)
result = neighb.score(x_trainScale,y_train)
resultTest = neighb.score(x_testScale,y_test)
print(result)
print(resultTest)
predTrain = neighb.predict(x_trainScale)
predTest = neighb.predict(x_testScale)
MAEtest = mean_absolute_error(y_test,predTest)
MAEtrain = mean_absolute_error(y_train,predTrain)
print("MAE Train")
print(MAEtrain)
print("MAE Test")
print(MAEtest)
MSEtest = mean_squared_error(y_test,predTest)
MSEtrain = mean_squared_error(y_train,predTrain)
print("MSE Train")
print(MSEtest)
print("MSE Test")
print(MSEtrain)