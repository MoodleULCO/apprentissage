import pandas as pa
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix
bdd = pa.read_csv("./iris.csv")
del(bdd['Id'])
# NB Classe 3
print(bdd.Species.value_counts())
# Caractéristiques 4 float64
print(bdd.info())
# NB exemple 50 par Species
print(bdd.Species.value_counts())
# BDD remplis par Species
print(bdd)
# Mélange
train,test = train_test_split(bdd,shuffle=True)
x_train = train
y_train = train['Species']
del(x_train['Species'])
x_test = test
y_test = test['Species']
del(x_test['Species'])
neighb = KNeighborsClassifier(n_neighbors=15)
trainFit = neighb.fit(x_train,y_train)
result = neighb.score(x_train,y_train)
resultTest = neighb.score(x_test,y_test)
print(result)
print(resultTest)
predTrain = neighb.predict(x_train)
predTest = neighb.predict(x_test)
matixTrain = confusion_matrix(y_train,predTrain)
matrixTest = confusion_matrix(y_test,predTest)
print("Matrix Train")
print(matixTrain)
print("Matrix Test")
print(matrixTest)